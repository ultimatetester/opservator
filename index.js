import path from 'path';
import async from 'async';
import fs from 'fs';
import {EventEmitter} from 'events';
import {Minimatch} from 'minimatch';
import {default as readDirRecursive} from 'recursive-readdir';

class Opservator extends EventEmitter {
  constructor(path, options) {
    super();

    options = options || {};

    this.options = {
      filesystem: options.filesystem || fs,
      recursive: options.recursive || true,
      matchPattern: options.matchPattern || '**/*',
      usePolling: options.usePolling || true,
      pollingInterval: options.pollingInterval || 3000,
    };

    this.shouldStop = false;
    this.path = path;
    this.pollingTimeoutHandle = null;
    this.folderCache = [];
    this.fileMatches = new Minimatch(this.options.matchPattern);
  }

  start() {
    this.shouldStop = false;
    this.pollUpdate();
  };

  stop() {
    this.shouldStop = true;

    if (this.pollingTimeoutHandle !== null) {
      clearInterval(this.pollingTimeoutHandle);
    }
  };

  pollUpdate() {
    const self = this;

    if (self.shouldStop === true) {
      return;
    }

    const fs = this.options.filesystem;
    const readdir = this.options.recursive ? readDirRecursive : fs.readdir.bind(fs);

    readdir(this.path, {fs: fs}, function(err, files) {
      if (err) {
        console.error(err);
        self.pollingTimeoutHandle = setTimeout(self.pollUpdate.bind(self), self.options.pollingInterval);
        return;
      }

      const newFolderCache = [];
      const statTasks = [];

      for (let i = 0; i < files.length; i++) {
        if (self.fileMatches.match(files[i]) === false) {
          continue;
        }

        const fileObject = {
          id: 0,
          path: files[i],
          old_path: '',
          size: 0,
          creation_date: '',
          change_date: '',
          is_directory: false,
        };

        statTasks.push(function(callback) {
          fs.stat(files[this.i], function(err, stats) {
            if (err) {
              return callback(null, err);
            }

            this.id = stats.ino;
            this.size = stats.size;
            this.creation_date = new Date(stats.birthtime);
            this.change_date = new Date(stats.mtime);
            this.is_directory = stats.isDirectory();

            callback(null);
          }.bind(this.o));
        }.bind({i: i, o: fileObject}));

        newFolderCache.push(fileObject);
      }

      async.parallel(statTasks, function(err) {
        if (err) {
          console.error(err);
          return;
        }

        self.compareFolderCaches(newFolderCache);
        self.pollingTimeoutHandle = setTimeout(self.pollUpdate.bind(self), self.options.pollingInterval);
      });
    });
  };

  compareFolderCaches(newCache) {
    const oldCache = this.folderCache.slice(0);
    this.folderCache = newCache.slice(0);

    for (let i = 0; i < newCache.length; i++) {
      const newEntry = newCache[i];
      let fileIsNew = true;
      let fileRenamed = false;
      let fileChanged = false;

      for (let j = oldCache.length - 1; j >= 0; j--) {
        const oldEntry = oldCache[j];

        if (newEntry.id === oldEntry.id) {
          oldCache.splice(j, 1);
          fileIsNew = false;

          // Let's find out if anything changed in the meantime!
          if (newEntry.path !== oldEntry.path) {
            newEntry.old_path = oldEntry.path;
            fileRenamed = true;
          }

          if (newEntry.change_date.getTime() !== oldEntry.change_date.getTime()) {
            fileChanged = true;
          }

          break;
        }
      }

      if (fileIsNew === true) {
        this.emit('add', newEntry);
      }

      if (fileRenamed === true) {
        this.emit('rename', newEntry);
      }

      if (fileChanged === true) {
        this.emit('change', newEntry);
      }
    }

    // Everything remaining in oldCache must have been deleted!
    for (let i = oldCache.length - 1; i >= 0; i--) {
      const deletedEntry = oldCache[i];
      this.emit('remove', deletedEntry);
    }
  }
}

export default Opservator;