# Opservator

Opservator is a simple package that is able to monitor any supplied filesystem and triggers events to inform the user of changes.
The difference between the native watching capability of NodeJS (i.e. fs.watch, fs.watchFile) is that these are hard to implement on non-native fs modules.
Other modules that are fs-like (e.g. fs-webdav, fs-backblaze, etc.) have an interface identical to the native fs but cannot implement the watch methods.
This module will solve exactly that. It will take any other module that has an interface identical to the native fs, and add the ability to monitor it.

### Usage

Create the instance

`const opservator = new Opservator(directoryPath, {
filesystem: fs, // This could be require('fs') but also any other module that shares the same interface as fs
pollingInterval: 3000, // Interval in ms
matchPattern: '**/*' // A minimatch pattern
});`

Setup event handler

`self.opservator.on('add', function (info) {
});`

Possible events are `add`, `rename`, `change` and `remove`